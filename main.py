from pyrogram import Client, MessageHandler
import time

api_id = open('config.txt').readlines()[0]
api_hash = open('config.txt').readlines()[1]


app = Client("my_account", api_id=api_id, api_hash=api_hash)


def get_message(client, message):
    print(message.message_id, message.chat.id)
    message.forward(message.chat.id, as_copy=True)


app.add_handler(MessageHandler(get_message))
app.start()






