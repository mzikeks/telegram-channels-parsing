from natasha import AddrExtractor, MorphVocab, NewsNERTagger, NewsEmbedding, Doc, Segmenter, NewsMorphTagger

emb = NewsEmbedding()
ner_tagger = NewsNERTagger(emb)
morph_tagger = NewsMorphTagger(emb)
segmenter = Segmenter()
morph_vocab = MorphVocab()
addr_extractor = AddrExtractor(morph_vocab)

lines = 'Свадебный кортеж из 14 тяжелогрузов собрали на свадьбе в Дагестане. Такой интересный вариант придумал жених из Карабудахкента (он дальнобойщик). 14 тягачей пригнали со всего Дагестана, путь в ЗАГС вышел роскошным.главное, чтоб не трансформеры'

doc = Doc(lines)
doc.segment(segmenter)
doc.tag_morph(morph_tagger)
doc.tag_ner(ner_tagger)

for span in doc.spans:
    span.normalize(morph_vocab)

post_location = []
for el in doc.spans:
    if el.type == 'LOC':
        post_location.append(el.normal)
print(post_location)
print(addr_extractor.find(', '.join(post_location)))