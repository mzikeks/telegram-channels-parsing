from telethon import TelegramClient
from telethon.tl.functions.channels import JoinChannelRequest
from sqlalchemy import create_engine, MetaData
import time
from random import randint

engine = create_engine('sqlite:///Channels.db')
metadata = MetaData()
metadata.reflect(bind=engine)
connection = engine.connect()

channels_table = metadata.sorted_tables[0]
channels_list = connection.execute(channels_table.select())


api_id = int(open('config.txt').readlines()[0])
api_hash = open('config.txt').readlines()[1]

with TelegramClient("my_account", api_id, api_hash) as client:
    for i, channel in enumerate(channels_list):
        username, name, category = channel
        client(JoinChannelRequest(username))
        time_to_sleep = randint(100, 1000)/10000
        print('%s: Вступил в канал %s (%d из %d), отдыхаю %s секунд' %
              (str(round(time.clock(), 2)), username, i+1, 6158, str(time_to_sleep)))
        time.sleep(time_to_sleep)
