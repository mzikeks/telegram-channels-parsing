import requests
from bs4 import BeautifulSoup
import time
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.keys import Keys

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import base
from Channel import Channel

engine = create_engine('sqlite:///Parsing.db')
base.Base.metadata.create_all(engine, checkfirst=True)
session = sessionmaker(bind=engine)()

opts = Options()
opts.set_headless()
assert opts.headless  # без графического интерфейса.
browser = webdriver.Firefox(options=opts)

start_time = time.time()

home_url = 'https://tlgrm.ru/channels'
home_ans = requests.get(home_url).text
soup = BeautifulSoup(home_ans, 'html.parser')
categories = soup.findAll(attrs={'class': 'channel-category'})

for i, category in enumerate(categories):
    time1 = time.time()
    category_name = category.text.strip()
    category_url = category['href']
    print(category_url, category_name, i, '/', len(categories))

    browser.get(category_url)
    for j in range(100):  # прокручиваем страницу до конца, для загрузки всех каналов
        browser.find_element_by_tag_name('body').send_keys(Keys.END)
        time.sleep(0.5)

    soup = BeautifulSoup(browser.page_source, 'html.parser')
    channels = soup.findAll(attrs={'class': 'channel-card__info'})

    for channel in channels[1:]: # первый - всегда рекламный
        name = channel.find(attrs={'class': 'channel-card__title'}).text.strip()
        username = channel.find(attrs={'class': 'channel-card__username'}).text.strip()
        username = username.replace('&commat', '@')
        if not session.query(Channel).filter_by(username=username).scalar():
            new_channel = Channel(name=name, username=username, category=category_name)
            session.add(new_channel)
            session.commit()

    print(time.time()-start_time)
