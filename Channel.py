from base import Base
from sqlalchemy import Column, String


class Channel(Base):
    __tablename__ = 'channels'

    username = Column(String, primary_key=True)
    name = Column(String)
    category = Column(String)
