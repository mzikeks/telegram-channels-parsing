from base import Base
from sqlalchemy import Column, String, BLOB, BigInteger, Integer, ForeignKey
from sqlalchemy.orm import relationship


class Image(Base):
    __tablename__ = 'images'
    __table_args__ = {'sqlite_autoincrement': True}

    id = Column(BigInteger().with_variant(Integer, 'sqlite'), primary_key=True)
    post_id = Column(String, ForeignKey('posts.id'))
    image_url = Column(String)
    image = Column(BLOB)
    post = relationship('Post', back_populates='images')
