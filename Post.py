from base import Base
from sqlalchemy import Column, String, ForeignKey
from sqlalchemy.orm import relationship


class Post(Base):
    __tablename__ = 'posts'

    id = Column(String, primary_key=True)
    source = Column(String)
    text = Column(String)
    category = Column(String)
    region = Column(String)
    images = relationship('Image', back_populates='post')



