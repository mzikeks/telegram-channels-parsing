from sqlalchemy import create_engine, ARRAY, String
from sqlalchemy.orm import sessionmaker
import requests
from bs4 import BeautifulSoup
import time
from Channel import Channel
from Post import Post
from Image import Image
import base
import os
from natasha import AddrExtractor, MorphVocab, NewsNERTagger, NewsEmbedding, Doc, Segmenter, NewsMorphTagger


# необходимое для разметки региона у поста
emb = NewsEmbedding()
ner_tagger = NewsNERTagger(emb)
morph_tagger = NewsMorphTagger(emb)
segmenter = Segmenter()
morph_vocab = MorphVocab()


engine = create_engine('sqlite:///Parsing.db')
base.Base.metadata.create_all(engine, checkfirst=True)
session = sessionmaker(bind=engine)()

channels_list = list(session.query(Channel))


regions_list = open('regions.txt', 'r', encoding='utf-8').readlines()
regions_list = [r.strip().split() for r in regions_list]


def write_log(text):
    log = open('log.txt', 'a')
    log.write(text + '\n')
    log.close()


write_log('Start at '+time.ctime())


def add_post(data, source, text, category, region):
    new_post = Post(id=data, source=source, text=text, category=category, region=region)
    session.add(new_post)
    session.commit()


def add_image(post_id, image_url, image):
    new_image = Image(post_id=post_id, image_url=image_url, image=image)
    session.add(new_image)
    session.commit()


n_cycle = 1

while True:
    start_time = time.time()
    db_size = os.path.getsize('Parsing.db')

    for i, channel in enumerate(channels_list):
        try:
            url = 'https://t.me/s/'+channel.username[1:]
            soup = BeautifulSoup(requests.get(url).text, 'html.parser')
            posts = soup.findAll(attrs={'class': 'tgme_widget_message force_userpic js-widget_message'})[::-1]

            for post in posts:
                post_id = post['data-post']
                post_text = post.find(attrs={'class': 'tgme_widget_message_text js-message_text'})

                post_document = Doc(str(post_text.text))
                post_document.segment(segmenter)
                post_document.tag_morph(morph_tagger)
                post_document.tag_ner(ner_tagger)

                for span in post_document.spans:
                    span.normalize(morph_vocab)

                post_location = []
                for el in post_document.spans:
                    if el.type == 'LOC':
                        for reg in regions_list:
                            reg_n = reg[0]
                            for sub_reg in reg[1:]:
                                if sub_reg.lower() == el.normal.lower():
                                    post_location.append(reg_n)

                post_region = "None"
                if post_location:
                    if len(set(post_location)) > 3:
                        post_region = '99'  # несколько регионов

                    else:
                        post_location.sort(key=lambda x: post_location.count(x), reverse=True)
                        post_region = post_location[0]

                if post_text:
                    post_text = post_text.text

                if session.query(Post).filter_by(id=post_id).scalar():  # если этот пост уже в БД
                    break  # то и все предыдущие тоже

                add_post(post_id, channel.username, post_text, channel.category, post_region)

                post_images = post.findAll(attrs={'class': 'tgme_widget_message_photo_wrap'})
                for post_image in post_images:
                    post_image = post_image['style']
                    post_image_url = post_image[post_image.index('https'):-2]

                    image_data = requests.get(post_image_url).content
                    add_image(post_id, post_image_url, image_data)

        except Exception as e:
            write_log(str(e))
            write_log('Time of error:' + time.ctime())
            time.sleep(250)

        print('Parse Channel: %s out of %s         Time after start cycle %s'
              % (i + 1, len(channels_list), time.time() - start_time))

    add_data_size = round((os.path.getsize('Parsing.db')-db_size)/1024/1024, 2)
    write_log('Cycle %s end       time of work %s     written to DB %s MB'
              % (n_cycle, round(time.time()-start_time, 2), add_data_size))

    n_cycle += 1
